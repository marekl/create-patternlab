/* eslint-disable import/no-extraneous-dependencies */
const retry = require('async-retry')
const chalk = require('chalk')
const path = require('path')

const got = require('got')
const tar = require('tar');
const { Stream } = require('stream')
const { promisify } = require('util')

const isWriteable = require('./helpers/is-writeable')
const makeDir = require('./helpers/make-dir')
const isFolderEmpty = require('./helpers/is-folder-empty')
const install = require('./helpers/install')
const writePackage = require('./helpers/write-package')
const writeThemeHeader = require('./helpers/write-theme-header')

const { tryGitInit } = require('./helpers/git')

const pipeline = promisify(Stream.pipeline)

function downloadAndExtractRepo(root, url) {
  return pipeline(
    got.stream(url),
    tar.extract(
      { cwd: root, strip: 1 },
    )
  )
}

async function createApp(appPath, appName, displayName, description, edition) {

  const root = path.resolve(appPath)

  if (!(await isWriteable(path.dirname(root)))) {
    console.error(
      'The application path is not writable, please check folder permissions and try again.'
    )
    console.error(
      'It is likely you do not have write permissions for this folder.'
    )
    process.exit(1)
  }

  const destFolder = path.basename(root)

  await makeDir(root)
  if (!isFolderEmpty(root, destFolder)) {
    process.exit(1)
  }

  const originalDirectory = process.cwd()

  const displayedCommand = 'npm'
  console.log(`Creating app in ${chalk.green(root)}.`)
  console.log()

  await makeDir(root)
  process.chdir(root)

  let repo, repoUrl = "";
  switch (edition) {
    case "wordpress":
      repo = "gitlab.com/marekl/patternlab-edition-wordpress.git"
      repoUrl = "https://gitlab.com/api/v4/projects/23474720/repository/archive.tar.gz"
      break;

    case "css-package":
      repo = "gitlab.com/marekl/patternlab-edition-css-package.git"
      repoUrl = "https://gitlab.com/api/v4/projects/25117924/repository/archive.tar.gz"
      break;

    default:
      throw new Error("Invalid edition")
  }

  console.log(
    `Downloading files from repo ${chalk.cyan(
      repo
    )}. This might take a moment.`
  )
  console.log()

  await retry(() => downloadAndExtractRepo(root, repoUrl), {
    retries: 3,
  })

  const version = '0.1.0'

  writePackage(root, { name: appName, description: description, version: version })
  if (edition === "wordpress")
    writeThemeHeader(root, "source/theme.css", displayName, description, version)

  console.log('Installing packages. This might take a couple of minutes.')
  console.log()

  await install(root, null)
  console.log()

  if (tryGitInit(root)) {
    console.log('Initialized a git repository.')
    console.log()
  }

  let cdpath = ""
  if (path.join(originalDirectory, destFolder) === appPath) {
    cdpath = destFolder
  } else {
    cdpath = appPath
  }

  console.log(`${chalk.green('Success!')} Created ${appName} at ${appPath}`)
  console.log('Inside that directory, you can run several commands:')
  console.log()
  console.log(chalk.cyan(`  ${displayedCommand} start`))
  console.log('    Starts the patternlab server.')
  console.log()
  if (edition === "wordpress") {
    console.log(chalk.cyan(`  ${displayedCommand} run wp`))
    console.log('    Starts the wordpress docker container.')
    console.log()
  }
}

module.exports = createApp