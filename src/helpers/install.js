const spawn = require('cross-spawn')

module.exports = function (
  root,
  dependencies
) {
  return new Promise((resolve, reject) => {
    let command
    let args
    command = 'npm'
    args = ([
      'install',
      dependencies && '--save',
      dependencies && '--save-exact',
      '--loglevel',
      'error',
    ].filter(Boolean)).concat(dependencies || [])

    const child = spawn(command, args, {
      stdio: 'inherit',
      env: { ...process.env, ADBLOCK: '1', DISABLE_OPENCOLLECTIVE: '1' },
    })
    child.on('close', (code) => {
      if (code !== 0) {
        reject({ command: `${command} ${args.join(' ')}` })
        return
      }
      resolve()
    })
  })
}
