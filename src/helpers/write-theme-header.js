const fs = require('fs');
const path = require('path');
const os = require('os');

function writeThemeHeader(root, filePath, displayName, description, version) {
    const header = `/*
Theme Name: ${displayName}
Description: ${description}
Version: ${version}
*/`

    fs.writeFileSync(
        path.resolve(root, filePath),
        header + os.EOL
    )
}

module.exports = writeThemeHeader