const fs = require('fs')

function makeDir(
  root,
  options = { recursive: true }
) {
  return fs.promises.mkdir(root, options)
}

module.exports = makeDir