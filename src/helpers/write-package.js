const fs = require('fs');
const path = require('path');
const os = require('os');

function writePackage(root, values) {
    const pkg = require(path.resolve(root, 'package.json'));

    const newPkg = { ...pkg, ...values }

    fs.writeFileSync(
        path.resolve(root, 'package.json'),
        JSON.stringify(newPkg, null, 2) + os.EOL
    )
}

module.exports = writePackage