#!/usr/bin/env node
/* eslint-disable import/no-extraneous-dependencies */
const chalk = require('chalk')
const Commander = require('commander')
const path = require('path')
const prompts = require('prompts')
const createApp = require('./create-app')
const packageJson = require('../package.json')

let projectPath = ''
let description = ''
let displayName = ''
let appName = ''

const program = new Commander.Command(packageJson.name)
  .version(packageJson.version)
  .arguments('<project-directory>')
  .usage(`${chalk.green('<project-directory>')} [options]`)
  .action((name) => {
    projectPath = name
  })
  .allowUnknownOption()
  .parse(process.argv)

async function run() {
  if (typeof projectPath === 'string') {
    projectPath = projectPath.trim()
  }

  const pathRes = await prompts({
    type: 'text',
    name: 'path',
    message: 'Name of the package',
    initial: projectPath || 'patternlab',
  })

  if (typeof pathRes.path === 'string') {
    appName = pathRes.path.trim()
  }

  if (!projectPath) {
    // Remove namespace
    projectPath = (/^(?:@.*?\/)?(.*)$/).exec(appName)[1]
  }


  if (!projectPath || !appName) {
    console.log()
    console.log('Please specify the project directory and name:')
    console.log(
      `  ${chalk.cyan(program.name())} ${chalk.green('<project-directory>')}`
    )
    console.log()
    console.log(
      `Run ${chalk.cyan(`${program.name()} --help`)} to see all options.`
    )
    process.exit(1)
  }

  const res = await prompts([
    {
      type: 'text',
      name: 'displayName',
      message: 'Display name',
      initial: projectPath,
    }, {
      type: 'text',
      name: 'description',
      message: 'Description of the project',
      initial: '',
    },
    {
      type: 'select',
      name: 'edition',
      message: 'Pick an edition',
      choices: [
        { title: 'Wordpress', value: 'wordpress' },
        { title: 'CSS package', value: 'css-package' }
      ]
    }
  ])

  if (typeof res.description === 'string') {
    description = res.description.trim()
  }

  if (typeof res.displayName === 'string') {
    displayName = res.displayName.trim()
  }

  const edition = res.edition

  const resolvedProjectPath = path.resolve(projectPath)

  await createApp(resolvedProjectPath, appName, displayName, description, edition)
}

run()
  .catch(async (reason) => {
    console.log()
    console.log('Aborting installation.')
    if (reason.command) {
      console.log(`  ${chalk.cyan(reason.command)} has failed.`)
    } else {
      console.log(chalk.red('Unexpected error. Please report it as a bug:'))
      console.log(reason)
    }
    console.log()

    process.exit(1)
  })
